# http://zopecomponent.readthedocs.io/en/latest/narr.html
from zope import component
from zope import interface
from zope.interface import implementer


class IGreeter(interface.Interface):
    def greet(self):
        """say hello"""


@implementer(IGreeter)
class Greeter:

    def __init__(self, other="world"):
        self.other = other

    def greet(self):
        return f"Hello {self.other}"


def test_greeter():
    greet = Greeter('bob')
    component.provideUtility(greet, IGreeter, 'robert')

    robert = component.queryUtility(IGreeter, 'robert')
    assert 'Hello bob' == robert.greet()
